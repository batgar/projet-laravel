<h1> LoL Fighters </h1>

Le but est de pouvoir trouver un partenaire de jeu sur League of Legends.

Un Utilisateur doit s’inscrire pour rentrer ses informations, il peut donner des info comme le Champions qu’il “main” et le Rôles qu’il joue fréquemment.

Chaque Champion à une ou plusieurs Catégorie (mage, assassins, tireur...), une ou plusieurs Lane (top, jungle,mid, adc, support) et une Faction.

Les Admins seront là pour modérer et mettre à jour les informations concernant le site (Champions, Rôles, Catégories, Factions et Utilisateurs).

Un visiteur pourra faire une recherche sur les utilisateurs selon les critères (Rôle, rank du joueur : élo) qui auront complété leur profil avec l'API et une recherche sur les champions (fonction de tri + détail champion)


Les Utilisateurs ont a disposition un formulaire dans leur onglet profil qui leur permet de récupérer et d'actualiser les informations du jeu avec leur pseudo League of Legends. Une fois les informations complétés, l'utilisateur est visible sur la page d'accueil, il pourra contacter et se faire contacter (par mail) par les autres joueurs.


<h2> Installation du projet : </h2>

    git clone https://gitlab.com/batgar/projet-laravel.git 
    cd projet-laravel/LoL_Fighters
    cp .env.example .env

Modifier les variables du fichier .env (BD et MAIL)

Créer la base de données

    composer install
    php artisan storage:link
    php artisan migrate --seed
    php artisan key:generate
    php artisan serve

go to http://localhost:8000/search

Compte admin sans compte Legends of Legends (ne peut donc pas contacter) :
email : admin@admin.com
pssword : adminadmin

Compte user + admin :
email : batgar@batgar.com
pssword : batgar

Compte user :
email : user@user.com
pssword : useruser

<h2> Problèmes rencontrés </h2>

<h3>Problèmes subsistants</h3>

npm ne fonctionne pas chez Alexandre (Install ok mais impossible de lancer des fonctionnalité, même après re-install + npm clear cash)

<h3>Problèmes rencontrés et résolus</h3>

Problème pour la gestion des images, lien entre public/stockage et storage/app/public.

Difficulté à établir des requêtes sur un model avec ses relation en Many-to-Many.

Quelques difficultés à faire les requête sur l’api LoL
