<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            FactionSeeder::class,
            ChampionSeeder::class,
            CategorySeeder::class,
            LaneSeeder::class,
            UserSeeder::class,
            ChampionLaneSeeder::class,
            ChampionCategorySeeder::class
        ]);
    }
}
