<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin 
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('adminadmin'),
            'admin' => true
            ]);
        
        // User 

        DB::table('users')->insert([
            'name' => 'leplusofort',
            'email' => 'leplusofort@leplusofort.com',
            'password' => bcrypt('leplusofort'),
            'username_ig' => 'Agurin',
            'rank' => 'CHALLENGER 450 LP',
            'nb_victoire' => 1760,
            'nb_defaite' => 0,
            'lane_id' => 2,
            'champion_id' => 11,
            'admin' => false
            ]);


        DB::table('users')->insert([
            'name' => 'paslemeilleur',
            'email' => 'paslemeilleur@paslemeilleur.com',
            'password' => bcrypt('paslemeilleur'),
            'username_ig' => 'ClickJJ',
            'rank' => 'GRANDMASTER 350 LP',
            'nb_victoire' => 1000,
            'nb_defaite' => 300,
            'lane_id' => 4,
            'champion_id' => 12,
            'admin' => false
            ]);


        DB::table('users')->insert([
            'name' => 'immaster',
            'email' => 'immaster@immaster.com',
            'password' => bcrypt('immaster'),
            'username_ig' => 'iZeal',
            'rank' => 'MASTER 450 LP',
            'nb_victoire' => 500,
            'nb_defaite' => 200,
            'lane_id' => 1,
            'champion_id' => 5,
            'admin' => false
            ]);

        DB::table('users')->insert([
            'name' => 'R2D2',
            'email' => 'R2D2@R2D2.com',
            'password' => bcrypt('R2D2R2D2'),
            'username_ig' => 'LeΙouch',
            'rank' => 'DIAMOND IV 67 LP',
            'nb_victoire' => 210,
            'nb_defaite' => 146,
            'lane_id' => 3,
            'champion_id' => 16,
            'admin' => false
            ]);
        

        DB::table('users')->insert([
            'name' => 'Radvens',
            'email' => 'radvens@radvens.com',
            'password' => bcrypt('radvens'),
            'username_ig' => 'RadVens',
            'rank' => 'PLATINUM IV 75 LP',
            'nb_victoire' => 362,
            'nb_defaite' => 358,
            'lane_id' => 2,
            'champion_id' => 2,
            'admin' => true
            ]);

        DB::table('users')->insert([
            'name' => 'batgar',
            'email' => 'batgar@batgar.com',
            'password' => bcrypt('batgar'),
            'username_ig' => 'batgar',
            'rank' => 'GOLD IV 61 LP',
            'nb_victoire' => 71,
            'nb_defaite' => 87,
            'lane_id' => 3,
            'champion_id' => 1,
            'admin' => true
            ]);

            
        DB::table('users')->insert([
            'name' => 'SoonGold',
            'email' => 'silver@silver.com',
            'password' => bcrypt('silver'),
            'username_ig' => 'HMT77 2',
            'rank' => 'SILVER I 99 LP',
            'nb_victoire' => 99,
            'nb_defaite' => 100,
            'lane_id' => 2,
            'champion_id' => 14,
            'admin' => false
            ]);


        DB::table('users')->insert([
            'name' => 'bronzehere',
            'email' => 'bronze@bronze.com',
            'password' => bcrypt('useruser'),
            'username_ig' => 'MNT04',
            'rank' => 'BRONZE II 1 LP',
            'nb_victoire' => 40,
            'nb_defaite' => 70,
            'lane_id' => 4,
            'champion_id' => 6,
            'admin' => false
            ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@user.com',
            'password' => bcrypt('useruser'),
            'username_ig' => 'ToCcy',
            'rank' => 'IRON IV 0 LP',
            'nb_victoire' => 1,
            'nb_defaite' => 999,
            'lane_id' => 5,
            'champion_id' => 17,
            'admin' => false
            ]);

                
    }
}
