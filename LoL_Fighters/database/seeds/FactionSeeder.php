<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
        'Bandle',
        'Bilgewater',
        'Demacia',
        'Freljord',
        'Ionia',
        'Ixtal',
        'Le Néant',
        'Noxus',
        'Piltover',
        'Shurima',
        'Targon',
        'Zaun',
        'Îles obscures'];

        foreach ($names as $name) {
            DB::table('factions')->insert([
                'name' => $name,
                'image' => 'faction/'. Str::slug($name) . '.png'
                ]);
        }
    }
}
