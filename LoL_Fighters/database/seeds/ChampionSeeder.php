<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChampionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $champions = [
            'xerath' => 
                ['xerath',
                "Xerath est un mage transfiguré de la Shurima antique, un être d'énergie arcanique habitant les fragments 
                détruits d'un sarcophage magique. Pendant des millénaires, il est resté prisonnier des sables du désert, 
                mais le retour de Shurima l'a libéré de sa prison ancestrale. Devenu ivre de pouvoir, il 
                cherche désormais à reprendre ce qu'il croit lui être dû et à remplacer les civilisations arrivistes qui
                ont pris possession du monde par une nouvelle, façonnée à son image.",
                10],
            'kayn' => 
                ['kayn',
                "Expert inégalé dans la pratique de la magie des ombres, Shieda Kayn combat pour accomplir sa véritable destinée : mener un jour l'Ordre de l'ombre vers une ère nouvelle où Ionia régnera en maître. Avec audace, il manie Rhaast, une arme darkin douée de raison, sans jamais craindre la corruption de son corps et de son esprit. Il ne peut y avoir pour lui que deux fins possibles : soit Kayn pliera l'arme à sa volonté, soit la faux maléfique le consumera complètement, ouvrant la voie à la destruction de tout Runeterra.

                « L'enfant est parti. Seul le tueur reste. »",
                5],
            'aatrox' =>
                ['aatrox',
                "Dieu-guerrier déchu qui manqua de détruire Runeterra, Aatrox, comme les autres Darkin, a été lié à une 
                arme antique et emprisonné pendant des siècles.
                Il est maintenant libre.
                Désormais en possession d'un corps qu'il a approximativement transformé pour rappeler son ancienne forme, 
                l'Épée des Darkin cherche à assouvir sa vengeance apocalyptique :
                l'annihilation totale.",
                10],
            'ahri' => [
                'ahri',
                "Dotée d'un lien inné avec le pouvoir naturel de Runeterra, Ahri est une Vastaya capable de modeler la 
                magie pour en faire des orbes d'énergie pure. Elle aime plus que tout jouer avec ses proies en manipulant
                 leurs émotions avant de dévorer leur essence vitale. En dépit de sa nature de prédateur, Ahri n'est pas
                  sans empathie, car chaque âme qu'elle absorbe l'emplit de fragments de ses souvenirs.",
                5
            ], 
            'ornn' =>
                ['ornn',
                "Ornn est le demi-dieu de la forge et de l'artisanat à Freljord. Avec comme seule compagne la solitude, il travaille dans sa forge enfouie au plus profond des cavernes de lave de l'Âtre-foyer. C'est là qu'il fond et purifie les métaux nécessaires à la création d'objets de qualité inimitable. Tandis que d'autres divinités – Volibear avant tout – s'immiscent dans les affaires des mortels, Ornn ne sort de son antre que pour remettre ces déités à leur place, avec son fidèle marteau ou les pouvoirs du volcan lui-même.

                « J'en ai assez dit. »",
                4
            ],
            'tristana' =>
                ['tristana',
                "Si bon nombre d'autres yordles utilisent leur énergie débordante pour explorer, découvrir, inventer ou plus simplement jouer de mauvais tours, Tristana a toujours puisé son inspiration dans les récits des grands guerriers. Elle avait beaucoup entendu parler de Runeterra, de ses factions et de ses guerres, et elle pensait que les yordles aussi avaient l'étoffe des légendes. Dès son arrivée dans ce monde, elle s'équipa de son fidèle canon Boomer pour se jeter au combat avec un courage et un optimisme sans bornes.

                « Attention, le petit oiseau va sortir ! »",
                1
            ],
            'pyke' =>
                ['pyke',
                "Jeté en pâture aux léviathans par son équipage, Pyke s'est noyé… mais il n'est pas resté mort. Des années plus tard, un revenant légendaire connu sous le nom d'Éventreur des abysses hante les quais-abattoirs, rayant les noms d'une liste sans fin. Une minute... Vous me rappelez quelqu'un...",
                2
            ],
            'senna' =>
                ['senna',
                "Condamnée dès l'enfance à être hantée par la mystérieuse Brume noire, Senna rejoignit un ordre sacré connu sous le nom des Sentinelles de la lumière. Elle combattit vaillamment, avant de périr sous les coups du cruel spectre Thresh et de voir son âme emprisonnée dans sa lanterne. Refusant de perdre espoir, Senna apprit à utiliser la Brume à l'intérieur de la lanterne et revint à la vie, changée à jamais. Brandissant désormais la puissance des ténèbres comme de la lumière, Senna cherche à mettre fin à la Brume noire en la retournant contre elle-même, chaque tir de son arme permettant de libérer les âmes qui y sont perdues.

                « Les ombres embrassent la lumière, et je les embrasse à mon tour… »",
                3
            ],

            'janna' =>
                ['janna',
                "Armée de la puissance des grands vents de Runeterra, Janna est un mystérieux esprit élémentaire qui protège les parias de Zaun. Certains croient que la vie lui a été donnée par les suppliques des marins de Runeterra cherchant les bons vents des mers calmes, affrontant les courants traîtres ou bravant les typhons. Depuis, on implore sa protection jusque dans les profondeurs de Zaun, où Janna est devenue un fanal d'espoir pour ceux qui sont dans le besoin. Nul ne sait quand et où elle apparaîtra, mais ceux qui espèrent son aide sont rarement déçus.

                « Ne crains pas les vents du changement, ils seront toujours là pour te porter vers les sommets. »",
                12
            ],

            'qiyana' =>
                ['qiyana',
                "Dans la ville d'Ixaocan, perdue dans la jungle, Qiyana complote pour obtenir au prix du sang le siège révéré des Yun Tal. Dernière dans l'ordre de la succession, elle affronte ceux qui sont sur son chemin avec une absolue confiance en elle et une maîtrise exceptionnelle de la magie élémentaire. La terre elle-même obéit à ses ordres et Qiyana se considère comme la plus grande élémentaliste de l'histoire d'Ixaocan. À ce titre, elle pense mériter, non une ville, mais un empire.

                « Un jour, tout cela appartiendra à Ixaocan. Un glorieux empire dirigé par une glorieuse impératrice. »",
                6
            ],

            "kha'zix" =>
                ['khazix',
                "Dans la ville d'Ixaocan, perdue dans la jungle, Qiyana complote pour obtenir au prix du sang le siège révéré des Yun Tal. Dernière dans l'ordre de la succession, elle affronte ceux qui sont sur son chemin avec une absolue confiance en elle et une maîtrise exceptionnelle de la magie élémentaire. La terre elle-même obéit à ses ordres et Qiyana se considère comme la plus grande élémentaliste de l'histoire d'Ixaocan. À ce titre, elle pense mériter, non une ville, mais un empire.

                « Un jour, tout cela appartiendra à Ixaocan. Un glorieux empire dirigé par une glorieuse impératrice. »",
                7
            ],

            "draven" =>
                ['draven',
                "À Noxus, des guerriers appelés « soudards » s'affrontent dans des arènes. Le sang y coule à foison et leurs forces y sont éprouvées, mais aucun d'entre eux n'a jamais été aussi célèbre que Draven. Cet ancien soldat découvrit que le public appréciait particulièrement son sens du spectacle, sans parler des gerbes de sang que faisaient gicler ses haches tournoyantes. Épris de sa propre perfection, Draven terrasse tous ses adversaires pour s'assurer que son nom continue de résonner dans les arènes de l'empire.

                « Le meilleur dépend de mes exigences du jour. »",
                8
            ],

            "maokai" =>
                ['maokai',
                "Maokai est un immense tréant possédé par la colère, qui combat les horreurs impies des Îles obscures. Il est devenu l'incarnation de la vengeance après la destruction de ses terres par un cataclysme magique, n'échappant lui-même à l'état de mort-vivant que grâce à l'eau de la vie qui l'irrigue. Autrefois, Maokai était un paisible esprit de la nature ; aujourd'hui, il combat avec fureur le fléau mort-vivant qui accable les Îles obscures pour rendre à l'archipel son ancienne beauté.

                « Tout autour de moi, il n'y a que des coquilles vides, sans âme et sans peur... mais je vais leur enseigner la terreur. »",
                13
            ],

            
            "vi" =>
                ['vi',
                "Autrefois criminelle des quartiers louches de Zaun, Vi est une femme sans peur, mais pas sans reproche, au tempérament bouillant, impulsive, qui n'a qu'un respect mesuré pour les représentants de l'autorité. Vi a grandi seule et a développé d'excellents instincts de survie ainsi qu'un sens de l'humour assez caustique. Aujourd'hui, elle travaille avec les gendarmes de Piltover pour maintenir l'ordre et se sert pour cela de puissants gantelets Hextech qui n'ont peur de frapper ni les plus épais murs de briques ni les suspects.

                « C'est vraiment dommage. J'ai deux poings et tu n'as qu'un nez. »",
                9
            ],

            "aphelios" =>
                ['aphelios',
                "Émergeant des ombres au clair de lune, Aphelios abat ceux qui voudraient anéantir sa foi sans un mot ; ses armes et sa précision mortelle parlent pour lui. Bien qu'animé par un poison qui le rend muet, il est guidé par sa sœur, Alune, qui depuis son lointain temple sacré lui confère un arsenal d'armes en pierre de lune. Tant que la lune brillera dans le ciel, Aphelios ne sera jamais seul.

                « Tous ceux qui renient notre foi seront rattrapés par notre destin. »",
                11
            ],

            "cassiopeia" =>
                ['cassiopeia',
                "Cassiopeia est une créature meurtrière qui excelle dans l'art de la manipulation. Elle était la plus jeune et la plus belle des filles de la famille noxienne Du Couteau, mais le jour où elle s'aventura dans les cryptes de Shurima en quête d'un pouvoir antique, le venin d'un gardien la transforma en un prédateur reptilien. Aussi rusée qu'agile, Cassiopeia se faufile désormais dans les ombres de la nuit, pétrifiant ses ennemis d'un simple regard.

                « Il n'y a pas d'antidote contre moi. »",
                8
            ],

            "yasuo" =>
                ['yasuo',
                "L'Ionien Yasuo est un épéiste agile et résolu, entraîné pour déchaîner contre ses ennemis la puissance de l'air. Quand il était jeune, la vanité le conduisit sur les chemins de la perdition, ce qui lui coûta sa position, son mentor et finalement son propre frère. Déshonoré par des accusations mensongères et désormais poursuivi comme un criminel, Yasuo erre sur ses terres natales, cherchant la rédemption en laissant le vent guider sa lame.

                « L'histoire d'une épée s'écrit avec du sang. »",
                5
            ],

            "lee sin" =>
                ['lee-sin',
                "Maître des antiques arts martiaux d'Ionia, Lee Sin est un combattant dévoué à de nobles principes qui canalise l'esprit du dragon pour affronter tous les défis. Bien qu'il ait perdu la vue il y a bien des années, le moine-guerrier a dédié sa vie à la protection de sa terre natale contre tous ceux qui voudraient en déstabiliser l'équilibre spirituel. Les ennemis qui sous-estiment son apparence méditative auront peu de temps pour le regretter, face à ses poings de feu et à ses coups de pied flamboyants.

                « Les actions d'une personne peuvent briser le monde, mais l'abnégation de plusieurs peut le rebâtir. »",
                5
            ],

            


            ];

    foreach ($champions as $name => $value) {
        
        DB::table('champions')->insert([
            'name' => $name,
            'slug' => $value[0],
            'description' => $value[1],
            'faction_id' => $value[2],
            'image_list' => 'champList/'. Str::slug($name) . '.png',
            'image_splash' => 'champSplashArt/'. Str::slug($name) . '.jpg',
            ]);
        }
    }
}
