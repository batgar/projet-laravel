<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lanes = [
            'top',
            'jungle',
            'mid',
            'adc',
            'support',
            ];
    
        foreach ($lanes as $lane) {
            DB::table('lane')->insert([
                'name' => $lane,
                'image' => 'roles/'. $lane .'.png'
                ]);
        }
    }
}
