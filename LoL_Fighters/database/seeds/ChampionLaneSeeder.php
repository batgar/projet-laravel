<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChampionLaneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('champion_lane')->insert([
            'champion_id' => 1,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 1,
            'lane_id' => 5
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 2,
            'lane_id' => 2
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 3,
            'lane_id' => 1
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 4,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 5,
            'lane_id' => 1
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 6,
            'lane_id' => 4
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 7,
            'lane_id' => 5
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 7,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 8,
            'lane_id' => 5
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 8,
            'lane_id' => 4
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 9,
            'lane_id' => 5
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 10,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 10,
            'lane_id' => 2
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 11,
            'lane_id' => 2
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 12,
            'lane_id' => 4
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 13,
            'lane_id' => 1
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 13,
            'lane_id' => 5
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 14,
            'lane_id' => 2
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 15,
            'lane_id' => 4
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 16,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 16,
            'lane_id' => 1
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 17,
            'lane_id' => 3
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 17,
            'lane_id' => 1
        ]);

        DB::table('champion_lane')->insert([
            'champion_id' => 18,
            'lane_id' => 2
        ]);


    }
}
