<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'assassin',
            'combattant',
            'mage',
            'tireur',
            'support',
            'tank'
            ];
    
        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'name' => $category,
                'image' => 'category/'. $category . '.png'
                ]);
        }
    }
}
