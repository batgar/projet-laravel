<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableChampionCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champion_category', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('champion_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign('champion_id')
                    ->references('id')
                    ->on('champions')
                    ->cascadeOnDelete();

            $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->cascadeOnDelete();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champion_category');
    }
}
