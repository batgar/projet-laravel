<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoIgTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username_ig')->nullable();
            $table->string('rank')->nullable();
            $table->integer('nb_victoire')->nullable();
            $table->integer('nb_defaite')->nullable();
            
            $table->unsignedBigInteger('lane_id')->nullable();
            $table->foreign('lane_id')
                    ->references('id')
                    ->on('lane')
                    ->cascadeOnDelete();

            $table->unsignedBigInteger('champion_id')->nullable();
            $table->foreign('champion_id')
                    ->references('id')
                    ->on('champions')
                    ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_lane_id_foreign');
            $table->dropForeign('users_champion_id_foreign');
        });
    }
}
