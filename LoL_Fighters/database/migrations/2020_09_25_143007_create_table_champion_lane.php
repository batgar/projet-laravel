<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableChampionLane extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('champion_lane', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('champion_id');
            $table->unsignedBigInteger('lane_id');

            $table->foreign('champion_id')
                    ->references('id')
                    ->on('champions')
                    ->cascadeOnDelete();

            $table->foreign('lane_id')
                    ->references('id')
                    ->on('lane')
                    ->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('champion_lane');
    }
}
