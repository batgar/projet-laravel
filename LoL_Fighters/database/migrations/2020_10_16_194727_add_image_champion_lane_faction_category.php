<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageChampionLaneFactionCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lane', function (Blueprint $table) {
            $table->string('image')->nullable();
        });

        Schema::table('factions', function (Blueprint $table) {
            $table->string('image')->nullable();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->string('image')->nullable();
        });

        Schema::table('champions', function (Blueprint $table) {
            $table->string('image_list')->nullable();
            $table->string('image_splash')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
