<?php

namespace App\Http\Middleware;

use Closure;

class HaveUsername_ig
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()) {   
            if ($request->user()->username_ig !== null) {
                return $next($request);
            }
        }
        return redirect()->route('home');
    }
}
