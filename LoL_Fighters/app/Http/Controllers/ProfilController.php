<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\LaneRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ChampionRepository;

class ProfilController extends Controller
{
    private $userRepository;
    private $laneRepository;
    private $championRepository;
    
    public function __construct(UserRepository $userRepository, LaneRepository $laneRepository, ChampionRepository $championRepository)   
    {
        $this->userRepository = $userRepository;
        $this->laneRepository = $laneRepository;
        $this->championRepository = $championRepository;
    }
    
    public function index()
    {
        $lanes = $this->laneRepository->all();
        $champions = $this->championRepository->all();

        return view('profil',[
            'lanes' => $lanes,
            'champions' => $champions
            ]);
    }

    public function update(User $user, Request $request)
    {
       
        $summener_name = $request["name_ig"];
        
        if($summener_name == null)
        {
            $summener_name = $user->username_ig;
            $lane_id = $user->lane_id;
            $champion_id = $user->champion_id;
        }else{
            $lane_id = $request['lane'] ;
            $champion_id = $request['champion'] ;
        }


        
        $api_key = "RGAPI-46999f69-ce93-477b-9a93-04c6a41c077c";

        $json_url = "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/$summener_name?api_key=$api_key";
        
        function get_http_response_code($url) {
            $headers = get_headers($url);
            return substr($headers[0], 9, 3);
        }

        if(get_http_response_code($json_url) === "200"){
            $json = file_get_contents($json_url);
            $data = json_decode($json, TRUE);
    
            $summener_id = $data["id"];
    
            $json_url2 = "https://euw1.api.riotgames.com/lol/league/v4/entries/by-summoner/$summener_id?api_key=$api_key";
            $json2 = file_get_contents($json_url2);
            $data2 = json_decode($json2, TRUE);
    
            if($data2[0]["queueType"] == "RANKED_SOLO_5x5")
            {
                $soloQ = $data2[0];
            }else 
            {
                $soloQ = $data2[1];
            }
    
            $user_data = [
                'username_ig' => $summener_name,
                'rank' => $soloQ["tier"]." ".$soloQ["rank"]." ".$soloQ["leaguePoints"]." LP",
                'nb_victoire' => $soloQ["wins"],
                'nb_defaite' => $soloQ["losses"],
                'champion_id' => $champion_id,
                'lane_id' => $lane_id
            ];
            
    
            $this->userRepository->update($user, $user_data);
            
    
            return redirect()->route('profil',Auth::user()->id);
        }else{
            $lanes = $this->laneRepository->all();
            $champions = $this->championRepository->all();
            $user = $this->userRepository->get($user->id);
            $error = 1;

            return view('profil',[
                'user' => $user,
                'lanes' => $lanes,
                'champions' => $champions,
                'error' => $error
                ]);
            
        }
       
    }
}
