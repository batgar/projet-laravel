<?php

namespace App\Http\Controllers;

use App\Repositories\LaneRepository;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class HomeController extends Controller
{

    private $userRepository;
    private $laneRepository;
    
    public function __construct(UserRepository $userRepository, LaneRepository $laneRepository)   
    {
        $this->userRepository = $userRepository;
        $this->laneRepository = $laneRepository;
    }

    public function index(Request $request)
    {
        $lane = $request->route('lane');
        $rank = $request->route('rank');

        if ($lane === null || ($lane === "0" && $rank === "0") ){
            $users = $this->userRepository->all();
        } else {
            $users = $this->userRepository->getUserBySearch($lane, $rank);
        }

        $lanes = $this->laneRepository->all();

        return view('home')->with([
            'users' => $users,
            'lanes' => $lanes
            ]);
    }
}
