<?php

namespace App\Http\Controllers;

use App\Faction;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\FactionRequest;
use App\Repositories\FactionRepository;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateFactionRequest;

class FactionController extends Controller
{

    private $factionRepository;
    
    public function __construct(FactionRepository $factionRepository)   
    {
        $this->factionRepository = $factionRepository;
    }

    public function index()
    {
        $factions = $this->factionRepository->all();

        return view('admin.faction.index')
        ->with([
            'factions' => $factions
            ]);
    }

    public function create()
    {
        return view('admin.faction.create');
    }

    public function store(FactionRequest $factionRequest)
    {
        $name = Str::slug($factionRequest['name']);
        $this->factionRepository->store(["name" => $name, 'image' => 'faction/'. $name . '.png']);        
        
        $image = $factionRequest['image'];
        $image->move('storage/faction', $name.'.png');

        return redirect()->route('faction');

    }

    public function edit(Faction $faction)
    {
        return view('admin.faction.edit')->with('faction', $faction);
    }

    public function update(Faction $faction, UpdateFactionRequest $factionRequest)
    {
        $name = Str::slug($factionRequest['name']);

        $factionRequest['image'] = isset($factionRequest['image']) ? $factionRequest['image']:null;

        $isImage = $factionRequest['image'];

        $image_name = explode('/',$faction->image);
        $image_name = $image_name[sizeof($image_name) -1];

        if ( $isImage ==! null ) {
            // Remplacement de l'anciene image par la nouvelle
            $isImage->move('storage/faction', $image_name);
        }

        $faction_data = ['name' => $name];
        
        $this->factionRepository->update($faction, $faction_data);

        return redirect()->route('faction');
    }

    public function delete(int $faction)
    {
        $faction_object = Faction::findOrFail($faction);

        Storage::delete("public/".$faction_object->list);
        $this->factionRepository->delete($faction);

        return redirect()->route('faction');
    }
}
