<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;


class UserAdminController extends Controller
{
    private $userRepository;
    
    public function __construct(UserRepository $userRepository)   
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $users = $this->userRepository->all();

        // dd($users);
        return view('admin.user.index',[
            'users' => $users
            ]);
    }

    public function delete(int $user)
    {
        $this->userRepository->delete($user);

        return redirect()->route('user.index');
    }

    public function updateAdmin(User $user){
        
         $admin = $user->admin;

         if($admin === 0){
            $admin = 1;
         }else{
            $admin = 0;
         }

         $user_data = [
            'admin' => $admin,
        ];
        
        $this->userRepository->updateAdmin($user, $user_data);
        
        return redirect()->route('user.index');

     }
}
