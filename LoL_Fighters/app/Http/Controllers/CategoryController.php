<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Storage;
use App\Repositories\CategoryRepository;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryRepository->all();

        return view('admin.category.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $categoryRequest)
    {
        $name = Str::slug($categoryRequest['name']);
        $this->categoryRepository->store(["name" => $name, 'image' => 'category/'. $name . '.png']);        
        
        $image = $categoryRequest['image'];
        $image->move('storage/category', $name.'.png');

        return redirect()->route('category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $categoryRequest, Category $category)
    {
        $name = Str::slug($categoryRequest['name']);

        $categoryRequest['image'] = isset($categoryRequest['image']) ? $categoryRequest['image']:null;

        $isImage = $categoryRequest['image'];

        $image_name = explode('/',$category->image);
        $image_name = $image_name[sizeof($image_name) -1];

        if ( $isImage ==! null ) {
            // Remplacement de l'anciene image par la nouvelle
            $isImage->move('storage/category', $image_name);
        }

        $faction_data = ['name' => $name];
        
        $this->categoryRepository->update($category, $faction_data);

        return redirect()->route('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function delete(int $category)
    {
        $category_object = Category::findOrFail($category);
        
        Storage::delete("public/".$category_object->image);
        $this->categoryRepository->delete($category);

        return redirect()->route('category');
    }
}
