<?php

namespace App\Http\Controllers;

use App\Lane;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\LaneRequest;
use App\Repositories\LaneRepository;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UpdateLaneRequest;

class LaneController extends Controller
{

    private $laneRepository;
    
    public function __construct(LaneRepository $laneRepository)   
    {
        $this->laneRepository = $laneRepository;
    }


    public function index()
    {
        $lanes = $this->laneRepository->all();

        return view('admin.lane.index')
        ->with([
            'lanes' => $lanes
            ]);
    }

    public function create()
    {
        return view('admin.lane.create');
    }

    public function store(LaneRequest $laneRequest)
    {
        $name = Str::slug($laneRequest['name']);
        $this->laneRepository->store(["name" => $name, 'image' => 'roles/'. $name . '.png']);   
        
        $image = $laneRequest['image'];
        $image->move('storage/roles', $name.'.png');

        return redirect()->route('lane');

    }

    public function edit(Lane $lane)
    {
        return view('admin.lane.edit')->with('lane', $lane);
    }

    public function update(Lane $lane, UpdateLaneRequest $laneRequest)
    {
        $name = Str::slug($laneRequest['name']);

        $laneRequest['image'] = isset($laneRequest['image']) ? $laneRequest['image']:null;

        $isImage = $laneRequest['image'];
        $laneRequest['image'] = $lane->image;

        $image_name = explode('/',$lane->image);
        $image_name = $image_name[sizeof($image_name) -1];

        if ( $isImage ==! null ) {
            // Remplacement de l'anciene image par la nouvelle
            $isImage->move('storage/roles', $image_name);
        } 

        $lane_data = ['name' => $name];
        
        $this->laneRepository->update($lane, $lane_data);

        return redirect()->route('lane');
    }

    public function delete(int $lane)
    {
        $lane_object = Lane::findOrFail($lane);

        Storage::delete("public/".$lane_object->image);
        $this->laneRepository->delete($lane);

        return redirect()->route('lane');
    }
}
