<?php

namespace App\Http\Controllers;

use App\Category;
use App\Champion;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Repositories\LaneRepository;
use App\Http\Requests\ChampionRequest;
use App\Repositories\FactionRepository;
use Illuminate\Support\Facades\Storage;
use App\Repositories\CategoryRepository;
use App\Repositories\ChampionRepository;
use App\Http\Requests\ChampionUpdateRequest;

class ChampionController extends Controller
{

    private $championRepository;
    private $laneRepository;
    private $factionRepository;
    private $categoryRepository;

    public function __construct(ChampionRepository $championRepository, LaneRepository $laneRepository,FactionRepository $factionRepository, CategoryRepository $categoryRepository) {
        $this->championRepository = $championRepository;
        $this->laneRepository = $laneRepository;
        $this->factionRepository = $factionRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $lane = $request->route('lane');
        $category = $request->route('category');
        $faction = $request->route('faction');

        if ($lane === null || ($lane === "0" && $category === "0" && $faction === "0") ){
            $champions = $this->championRepository->all();
        } else {
            $champions = $this->championRepository->getChampionBySearch($lane, $category, $faction);
        }

        $lanes = $this->laneRepository->all();
        $factions = $this->factionRepository->all();
        $categories = $this->categoryRepository->all();
        return view('champion')
        ->with([
            'categories' => $categories,
            'champions' => $champions,
            'lanes' => $lanes,
            'factions' => $factions
            ]);
    }

    public function show(Champion $champion)
    {
        return view('detailsChampion')->with('champion', $champion);
    }

    public function create(LaneRepository $laneRepository, CategoryRepository $categoryRepository, FactionRepository $factionRepository)
    {
        $lanes = $laneRepository->all();
        $categories = $categoryRepository->all();
        $factions = $factionRepository->all();

        return view('admin.champion.create')->with(['lanes' => $lanes , 'categories' => $categories , 'factions' => $factions ]);
    }

    public function store(ChampionRequest $championRequest)
    {
        
        $categories = $championRequest['categories'];
        $champion_categories = [];
        foreach ($categories as $category_id) {
            array_push($champion_categories , $this->categoryRepository->get($category_id));
        }

        $lanes = $championRequest['lanes'];
        $champion_lanes = [];
        foreach ($lanes as $lane_id) {
            array_push($champion_lanes , $this->laneRepository->get($lane_id));
        }

        $slug = Str::slug($championRequest['name']);
        
        $data_champ = [
            "name" => $championRequest['name'],
            "description" => $championRequest['description'],
            "faction" => $championRequest['faction'],
            "image_list" => 'champList/'. $slug . '.png',
            "image_splash" => 'champSplashArt/'. $slug . '.jpg',
            "champion_categories" => $champion_categories,
            "champion_lane" => $champion_lanes
        ];

        $this->championRepository->store($data_champ);
        
        $image_list = $championRequest['image_list'];
        $image_list->move('storage/champList', $slug . '.png');
        
        $image_splash = $championRequest['image_splash'];
        $image_splash->move('storage/champSplashArt', $slug . '.jpg');
        
        return redirect()->route('champion');
    }

    public function edit(Champion $champion)
    {
        $list_cat_name = [];
        foreach ($champion->category as $category) {
            array_push($list_cat_name, $category->name);
        }
        $list_lane_name = [];
        foreach ($champion->lane as $lane) {
            array_push($list_lane_name, $lane->name);
        }
        $lanes = $this->laneRepository->all();
        $categories = $this->categoryRepository->all();
        $faction = $this->factionRepository->all();

        return view('admin.champion.edit')->with([
            'list_lane' => $list_lane_name,
            'list_cat' => $list_cat_name,
            'lanes' => $lanes,
            'categories' => $categories,
            'champion' => $champion,
            'factions' => $faction
            ]);
    }


    public function update(ChampionUpdateRequest $championRequest, Champion $champion)
    {
        $categories = $championRequest['categories'];
        $champion_categories = [];
        foreach ($categories as $category_id) {
            array_push($champion_categories , $this->categoryRepository->get($category_id));
        }

        $lanes = $championRequest['lanes'];
        $champion_lanes = [];
        foreach ($lanes as $lane_id) {
            array_push($champion_lanes , $this->laneRepository->get($lane_id));
        }
        
        $data_champ = [
            "name" => $championRequest['name'],
            "description" => $championRequest['description'],
            "faction" => $championRequest['faction'],
            "champion_categories" => $champion_categories,
            "champion_lane" => $champion_lanes
        ];

        $this->championRepository->update($champion, $data_champ);
        
        $isImage = $championRequest['image_list'];
        $image_list_name = explode('/',$champion->image_list);
        $image_list_name = $image_list_name[sizeof($image_list_name) -1];
        if ( $isImage ==! null ) {
            // Remplacement de l'anciene image par la nouvelle
            $isImage->move('storage/champList', $image_list_name);
        }

        $isImage = $championRequest['image_splash'];
        $image_splash_name = explode('/',$champion->image_splash);
        $image_splash_name = $image_splash_name[sizeof($image_splash_name) -1];
        if ( $isImage ==! null ) {
            // Remplacement de l'anciene image par la nouvelle
            $isImage->move('storage/champSplashArt', $image_splash_name);
        }

        return redirect()->route('champion');

    }

    public function delete(int $champion)
    {
        $champion_object = Champion::findOrFail($champion);

        Storage::delete("public/".$champion_object->image_list);
        Storage::delete("public/".$champion_object->image_splash);

        $this->championRepository->delete($champion);

        return redirect()->route('champion');
    }
}
