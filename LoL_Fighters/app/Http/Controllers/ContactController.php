<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Repositories\LaneRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use App\Repositories\ChampionRepository;

class ContactController extends Controller
{

    private $userRepository;
    private $laneRepository;
    private $championRepository;
    
    public function __construct(UserRepository $userRepository, LaneRepository $laneRepository, ChampionRepository $championRepository)   
    {
        $this->userRepository = $userRepository;
        $this->laneRepository = $laneRepository;
        $this->championRepository = $championRepository;
    } 
    public function index(User $user)
    {
        $lanes = $this->laneRepository->all();
        $champions = $this->championRepository->all();
        $user = $this->userRepository->get($user->id);

        return view('contact',[
            'user' => $user,
            'lanes' => $lanes,
            'champions' => $champions
            ]);
    }

    public function email(Request $request)
    {
        Mail::to('contact@mon-blog.com')->send(new ContactMail($request->all()));
        return response()->json(['success'=> 'success']);
    }
}
