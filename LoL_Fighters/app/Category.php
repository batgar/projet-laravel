<?php

namespace App;

use App\Champion;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillables = ['name'];
    public $timestamps = false;

    /**
     * The champion that belong to the category.
     */
    public function champion()
    {
        return $this->belongsToMany(Champion::class, 'champion_category', 'category_id', 'champion_id');
    }
}
