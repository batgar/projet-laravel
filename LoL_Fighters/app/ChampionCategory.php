<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChampionCategory extends Model
{
    protected $table = "champion_category";
}
