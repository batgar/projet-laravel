<?php

namespace App;

use App\Champion;
use Illuminate\Database\Eloquent\Model;

class Faction extends Model
{
    protected $table = "factions";
    protected $fillables = ['name'];
    public $timestamps = false;

    public function faction()
    {
        return $this->hasMany(Champion::class);
    }
}
