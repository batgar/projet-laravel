<?php

namespace App\Repositories;

use App\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{
    /**
     * Get a Category by his id
     * 
     * @param int category_id
     * @return Category
     */
    public function get(int $category_id): Category
    {
        return Category::find($category_id);
    }
    
    /**
    * Get all Category
    * 
    * @return Collection
    */
    public function all(): Collection
    {
        return Category::all();
    }

    /**
     * Delete a Category
     * 
     * @param int
     * 
     */
    public function delete(int $category): void
    {
        Category::destroy($category);
    }

    /**
     * Updates a Category.
     *
     * @param Category
     * @param array
     */
    public function update(Category $category, array $category_data)
    {
        $category->name = $category_data['name'];
        $category->save();
    }

    /**
     * Creer un Champion avec la liste des attribus donnés
     */
    public function store(array $category_data)
    {
        $category = new Category();
        $category->name = $category_data['name'];
        $category->image = $category_data['image'];
        $category->save();
    }
}