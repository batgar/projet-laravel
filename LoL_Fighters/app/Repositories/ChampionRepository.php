<?php

namespace App\Repositories;

use App\Category;
use App\Champion;
use App\ChampionCategory;
use App\ChampionLane;
use App\Http\Requests\ChampionRequest;
use App\Lane;
use Illuminate\Database\Eloquent\Collection;

class ChampionRepository
{
    /**
     * Get a Champion by his id
     * 
     * @param int champ_id
     * @return Collection
     */
    public function get(int $champ_id): Collection
    {
        return Champion::find($champ_id);
    }
    
    /**
    * Get all Champion
    * 
    * @return Collection
    */
    public function all(): Collection
    {
        return Champion::all();
    }

    public function getChampionBySearch(string $lane, string $category, string $faction)
    {
        if ($lane !== "0" && $category !== "0" && $faction !== "0") {
            
            $champion_category = ChampionCategory::where('category_id', '=', $category)->get('champion_id');
            $champion_lane = ChampionLane::where('lane_id', '=', $lane)->get('champion_id');
            $champion_faction = Champion::where('faction_id' , "=", $faction)->get();

            $champion_lane_list = [];
            $champion_category_list = [];
            $champion_faction_list = [];

            foreach ($champion_lane as $champ) {
                array_push($champion_lane_list, $champ->champion_id);
            }
            foreach ($champion_category as $champ) {
                array_push($champion_category_list, $champ->champion_id);
            }
            foreach ($champion_faction as $champ) {
                array_push($champion_faction_list, $champ->id);
            }


            $champions = array_intersect($champion_lane_list, $champion_category_list, $champion_faction_list);

            $list_champions = [];
            foreach ($champions as $key => $value) {
                array_push($list_champions, Champion::find($value));
            }

            return $list_champions;
        } 
        elseif ($lane !== "0" && $category !== "0") {

            $champion_category = ChampionCategory::where('category_id', '=', $category)->get('champion_id');
            $champion_lane = ChampionLane::where('lane_id', '=', $lane)->get('champion_id');

            $champion_lane_list = [];
            $champion_category_list = [];

            foreach ($champion_lane as $champ) {
                array_push($champion_lane_list, $champ);
            }
            foreach ($champion_category as $champ) {
                array_push($champion_category_list, $champ);
            }

            $champions = array_intersect($champion_lane_list, $champion_category_list);

            $list_champions = [];
            foreach ($champions as $key => $value) {
                array_push($list_champions, Champion::find($value->champion_id));
            }

            return $list_champions;

        } elseif ($lane !== "0" && $faction !== "0") {
            
            return Lane::find($lane)->champion()->where('faction_id' , "=", $faction)->get();
            
        } elseif ($category !== "0" && $faction !== "0") {
            
            return Category::find($category)->champion()->where('faction_id' , "=", $faction)->get();

        } elseif ($lane !== "0") {

            return Lane::find($lane)->champion()->get();

        } elseif ($category !== "0") {

            return Category::find($category)->champion()->get();

        } elseif ($faction !== "0") {

            return Champion::where('faction_id' , "=", $faction)->get();

        }
        return Champion::all();

    }

    /**
     * Delete a Champion
     * 
     * @param Champion
     * 
     */
    public function delete(int $champion): void
    {
        Champion::destroy($champion);
    }

    /**
     * Updates a champion.
     *
     * @param Champion
     * @param array
     */
    public function update(Champion $champion, array $champion_data)
    {
        $champion->name = $champion_data['name'];
        $champion->slug = $champion_data['name'];
        $champion->description = $champion_data['description'];
        $champion->faction_id = $champion_data['faction'];
        $champion->save();
        $champion->category()->detach();
        foreach ($champion_data['champion_categories'] as $category) {
            $champion->category()->save($category);
        }
        $champion->lane()->detach();
        foreach ($champion_data['champion_lane'] as $lane) {
            $champion->lane()->save($lane);
        }
    }

    /**
     * Creer un Champion avec la liste des attribus donnés
     */
    public function store(array $champion_data)
    {
        $champion = new Champion();
        $champion->name = $champion_data['name'];
        $champion->slug = $champion_data['name'];
        $champion->description = $champion_data['description'];
        $champion->faction_id = $champion_data['faction'];
        $champion->image_list = $champion_data['image_list'];
        $champion->image_splash = $champion_data['image_splash'];
        $champion->save();
        foreach ($champion_data['champion_categories'] as $category) {
            $champion->category()->save($category);
        }
        foreach ($champion_data['champion_lane'] as $lane) {
            $champion->lane()->save($lane);
        }
    }
}