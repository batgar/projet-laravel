<?php

namespace App\Repositories;

use App\Faction;
use Illuminate\Database\Eloquent\Collection;

class FactionRepository
{
    /**
     * Get a Champion by his id
     * 
     * @param int champ_id
     * @return Collection
     */
    public function get(int $faction_id): Collection
    {
        return Faction::find($faction_id);
    }
    
    /**
    * Get all Faction
    * 
    * @return Collection
    */
    public function all(): Collection
    {
        return Faction::all();
    }

    /**
     * Delete a Faction
     * 
     * @param Faction
     * 
     */
    public function delete(int $faction): void
    {
        Faction::destroy($faction);
    }

    /**
     * Updates a faction.
     *
     * @param Faction
     * @param array
     */
    public function update(Faction $faction, array $faction_data)
    {
        $faction->name = $faction_data['name'];
        $faction->save();
    }

    /**
     * Creer un Faction avec la liste des attribus donnés
     */
    public function create(array $faction_data): bool
    {
        $faction = new Faction();
        $faction->name = $faction_data['name'];

        return true;
    }

    public function store(array $faction_data)
    {
        $faction = new Faction();
        $faction->name = $faction_data['name'];
        $faction->image = $faction_data['image'];
        $faction->save();
    }
}