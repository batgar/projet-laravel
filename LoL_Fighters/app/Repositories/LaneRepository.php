<?php

namespace App\Repositories;

use App\Lane;
use Illuminate\Database\Eloquent\Collection;

class LaneRepository
{
    /**
     * Get a Lane by his id
     * 
     * @param int champ_id
     * @return Lane
     */
    public function get(int $lane_id): Lane
    {
        return Lane::find($lane_id);
    }
    
    /**
    * Get all lanes
    * 
    * @return Collection
    */
    public function all(): Collection
    {
        return Lane::all();
    }

    /**
     * Delete a Lane
     * 
     * @param Lane
     * 
     */
    public function delete(int $lane): void
    {
        Lane::destroy($lane);
    }

    /**
     * Updates a lane.
     *
     * @param Lane
     * @param array
     */
    public function update(Lane $lane, array $lane_data)
    {
        $lane->name = $lane_data['name'];
        $lane->save();
    }

    /**
     * Creer une Lane avec la liste des attribus donnés
     */
    public function store(array $lane_data)
    {
        $lane = new Lane();
        $lane->name = $lane_data['name'];
        $lane->image = $lane_data['image'];
        $lane->save();
    }
}