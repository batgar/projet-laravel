<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
{
    /**
     * Get a User by his id
     * 
     * @param int champ_id
     * @return Collection
     */
    public function get(int $user_id)
    {
        return User::find($user_id);
    }
    
    /**
    * Get all User
    * 
    * @return Collection
    */
    public function all(): Collection
    {
        return User::with('lane','champion')->get();
    }

    public function getUserBySearch(string $lane, string $rank)
    {
        if ($lane !== "0" && $rank !== "0") {
            return User::where('lane_id', '=', $lane )->where('rank', 'LIKE' , $rank.'%')->get();
        } elseif ($lane !== "0") {
            return User::where('lane_id', '=', $lane )->get();
        } elseif ($rank !== "0") {
            return User::where('rank', 'LIKE' , $rank.'%')->get();
        }
    }

    /**
     * Delete a User
     * 
     * @param User
     * 
     */
    public function delete(int $user): void
    {
        User::destroy($user);
    }

    /**
     * Updates a user.
     *
     * @param User
     * @param array
     */
    public function update(User $user, array $user_data)
    {
        $user->username_ig = $user_data["username_ig"];
        $user->rank = $user_data["rank"];
        $user->nb_victoire = $user_data["nb_victoire"];
        $user->nb_defaite = $user_data["nb_defaite"];
        $user->champion_id = $user_data["champion_id"];
        $user->lane_id = $user_data["lane_id"];

        $user->save();
    }

    public function updateAdmin(User $user, array $user_data)
    {
        $user->admin = $user_data["admin"];
     

        $user->save();
    }



}