<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChampionLane extends Model
{
    protected $table = "champion_lane";
}
