<?php

namespace App;

use App\Lane;
use App\User;
use App\Faction;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Champion extends Model
{
    protected $table = "champions";
    protected $fillables = ['name', 'slug', 'descritpion'];

    /**
     * The catgory that belong to the champion.
     */
    public function category()
    {
        return $this->belongsToMany(Category::class, 'champion_category', 'champion_id', 'category_id');
    }

    /**
     * The lane that belong to the lane.
     */
    public function lane()
    {
        return $this->belongsToMany(Lane::class, 'champion_lane', 'champion_id', 'lane_id');
    }

    public function faction()
    {
        return $this->belongsTo(Faction::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Mutator : permet de set le slug avec le name
     */
    public function setSlugAttribute($name)
    {
        $this->attributes['slug'] = Str::slug($name);
    }
}
