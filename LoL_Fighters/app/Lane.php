<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lane extends Model
{
    protected $table = "lane";
    protected $fillables = ['name'];
    public $timestamps = false;

    /**
     * The champion that belong to the lane.
     */
    public function champion()
    {
        return $this->belongsToMany(Champion::class,'champion_lane', 'lane_id', 'champion_id');
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }
}