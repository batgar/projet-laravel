@extends('layouts.app')

<style>
    .center-h-v{
        align-items: center; display:flex; justify-content:center;
    } 

    .shadow-border{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.20);
        background-color: rgb(240, 240, 240)
        
    }

    .shadow-border:hover{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.3);
    }
  
</style>

@section('content') 
<div class="row mt-5">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body shadow-border">
                <div class="row">                
                    <div class="col-md-12 mb-4">
                        <div class="card-body">
                            <h1>{{ ucfirst($champion->name) }}</h1>
                            <hr/>
                            <div class="text-center mt-4">
                                <img class="rounded" width="85%" src="{{asset('storage/'.$champion->image_splash)}}" alt="">
                            </div>
                            <h3 class="mt-5">Présentation</h2>
                            <hr/>
                            <p>
                                Faction : {{$champion->faction->name}}
                            </p>
                            <p class="mt-3">
                                {{$champion->description}}
                            </p>
                            <p>
                                Positions : 
                                @foreach ($champion->lane as $l)
                                    {{ ucfirst($l->name) }}
                                @endforeach
                            </p>
                            <p>
                                Catégories : 
                                @foreach ($champion->category as $c)
                                    {{ ucfirst($c->name) }}
                                @endforeach
                            </p>
                        </div>
                    </div>                
                </div>     
            </div>
        </div>
    </div>
</div>
@endsection
