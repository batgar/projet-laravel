@extends('layouts.app')

@section('content')

    <form class="mt-5" id="form-contact">

        <h1>Contact</h1>
        @csrf
        
        <div class="form-group">
            <input type="email" name="email" class="form-control" hidden value="{{Auth::user()->email}}" required>
            <input type="text" name="name" class="form-control" hidden value="{{Auth::user()->name}}" required>
            <input type="text" name="nametosend" class="form-control" hidden value="{{$user->name}}" required>
            <input type="text" name="username_ig" class="form-control" hidden value="{{Auth::user()->username_ig}}" required>
            
        </div>

        <div class="form-group">
            <label>Message</label>
            <textarea name="message" class="form-control" rows="5" required></textarea>
        </div>

        <button type="submit" id="btn-contact" class="btn btn-primary">Envoyer le message</button>
    </form>
    <script src="{{asset('js/contact.js')}}"></script>
@endsection