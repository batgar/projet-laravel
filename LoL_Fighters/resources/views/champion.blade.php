@extends('layouts.app')

@section('title') Recherche de joueurs @endsection
<style>
    .center-h-v{
        align-items: center; display:flex; justify-content:center;
    } 

    .shadow-border{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.20);
        background-color: rgb(240, 240, 240)
        
    }

    .shadow-border:hover{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.3);
    }

    .img-champ{
        width: 100%;
    }

    .bg-hover:hover{
        background-color: rgb(200, 200, 200);
    }

    .no-decoration{
        text-decoration: none;
        color:black;
        
    }

    .no-decoration:hover{
        text-decoration: none;
        color:black;
    }
</style>

@section('content')

<form onsubmit="return findChampionBySearch()" class="row text-center mt-5">       
    <div class="col-md-3">
        <div class="form-group">
            <label>Rôle</label>
            <select id="selectLane" class="form-control shadow-border">
                <option value="0">Tous</option>
                @foreach ($lanes as $lane)
                    <option value="{{$lane->id}}">{{ucfirst($lane->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Category</label>
            <select id="selectCategory" class="form-control shadow-border">
                <option value="0">Toutes</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{ucfirst($category->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label>Factions</label>
            <select id="selectFaction" class="form-control shadow-border">
                <option value="0">Toutes</option>
                @foreach ($factions->sortBy('name') as $faction)
                    <option value="{{$faction->id}}">{{$faction->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3 text-left pt-4 mt-2">
        <button class="btn btn-success" type="submit">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
            </svg>
        </button>
    </div>
</form>
<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body shadow-border">
                <div class="row">
                    @foreach ($champions as $champion)                    
                        <div class="col-md-2 mb-4">
                            <a class="no-decoration" href="{{route('detailsChampion', $champion)}}">
                                <div class="card">
                                    <div class="card-body shadow-border text-center bg-hover">
                                        <div class="w-100">
                                            <img class="img-champ rounded-circle shadow-border" src="{{asset('storage/'.$champion->image_list)}}" alt="">
                                            <br>
                                            <p class="mt-2 mb-0 ">{{ucfirst($champion->name)}}</p>        
                                        </div>
                                        <div class="w-100">
                                            @if ( Auth::user() )
                                                @if ( Auth::user()->admin)
                                                <a href="{{route('champion.edit', $champion)}}" type="button" class="w-100 btn btn-primary btn-sm btn-size mt-2">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                                    </svg>
                                                    Modifier
                                                </a>
                                                <form action="{{route('champion.delete', $champion->id)}}" method="post">
                                                    @method('DELETE')
                                                    @csrf              
                                                    <button  type="submit" class="w-100 btn btn-danger btn-sm btn-size mt-2">                   
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                        </svg>
                                                        Supprimer
                                                    </button>
                                                </form>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>                
                    @endforeach
                    @if (Auth::user())
                        @if (Auth::user()->admin)  
                            <div class="col-md-2 mb-4" >
                                <a class="no-decoration" href="{{route('champion.create')}}">
                                    <div class="card" style="height: 100%" >
                                        <div class="card-body shadow-border text-center bg-hover ">
                                            <div class="w-100">
                                                <div class="col-md-12 center-h-v" style="height: 50%">
                                                    <h6>Ajouter un champion</h6>
                                                </div>
                                                <div class="col-md-12" style="height: 50%">
                                                    <svg width="90%"  viewBox="0 0 16 16" class="text-primary bi bi-plus-circle-fill mb-5" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                                    </svg>
                                                </div>   
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endif           
                </div>     
            </div>
        </div>
    </div>
</div>
@endsection

<script>
function findChampionBySearch(){
    let idLane = $("#selectLane").val();
    let idCategory = $("#selectCategory").val();
    let idFaction = $("#selectFaction").val();

    window.location.href = "{{route('champion')}}/" + idLane + "/" + idCategory + "/" + idFaction;
    return false
}
</script>