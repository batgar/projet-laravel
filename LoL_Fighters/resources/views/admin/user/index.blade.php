@extends('layouts.app')

@section('datatables-Link')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div class="row mt-5">
    <div class="col-md-12 mb-4">
        <table id="data-table" class="row-border">  
            <thead >  
                <tr class="text-center">  
                    <th>Nom</th>
                    <th>Admin</th> 
                    <th>Nom en jeu</th> 
                    <th>Email</th>
                    <th>Action</th>
                    <th>Droits</th>
                </tr>  
            </thead>  
            <tbody>
                @foreach ($users as $user)
                    <tr class="text-center">
                        <td>{{$user->name}}</td>
                        <td>
                            <div hidden>{{$user->admin}}</div>
                            @if ($user->admin==1)
                                <svg width="2em" height="2em" viewBox="0 0 16 16" class="text-success bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                                </svg>  
                            @else
                                <svg width="2em" height="2em" viewBox="0 0 16 16" class="text-danger bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                </svg>
                            @endif
                        </td>
                        <td>{{$user->username_ig}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <form action="{{route('user.delete', $user->id)}}" method="post">
                                @method('DELETE')
                                @csrf              
                                <button style="width: 100%"  type="submit" class="btn btn-danger btn-sm pt-0 pb-0">
                                     Supprimer
                                </button>
                            </form>
                        </td>
                        <td>                          
                            <form action="{{route('user.update', $user)}}" method="post"> 
                                @csrf         
                                <button style="width: 100%"  type="submit" class="btn btn-warning btn-sm pt-0 pb-0">
                                    Admin={{$user->admin}}
                                </button>
                                @include('components.errors')
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>  
    </div>
</div>
@endsection
@section('script') 
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#data-table').DataTable({

            "language": {
                "url": "http://cdn.datatables.net/plug-ins/1.10.9/i18n/French.json"
            }
        });
    } );
    
</script>
@endsection

