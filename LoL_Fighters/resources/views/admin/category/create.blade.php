@extends('layouts.app')

@section('content')
<form class="mt-5" method="post" action="{{route('category.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nom de la category</label>
        <input type="text" name="name" class="form-control" required>
    </div>
    <div class="form-group">
        <label>Image</label>
        <div class="custom-file">           
            <input type="file" name="image" class="custom-file-input"  d="customFileLang" lang="fr"  accept="image/png" required>
            <label class="custom-file-label" for="customFile">Sélectionner une image</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block">AJOUTER</button>

    @include('components.errors')

</form>
@endsection