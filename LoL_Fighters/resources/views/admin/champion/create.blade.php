@extends('layouts.app')

@section('content')
<form class="mt-5" method="post" action="{{route('champion.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nom du champion</label>
        <input type="text" name="name" class="form-control" required value="{{old('name')}}">
    </div>
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control" name="description" cols="30" rows="10">{{old('description')}}</textarea>
    </div>
    <div class="form-group">
        <label>Faction</label>
        <select name="faction" class="form-control">
            @foreach ($factions as $faction)
                <option value="{{$faction->id}}">{{$faction->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Categories</label>
        <select name="categories[]" multiple class="form-control">
            @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Lanes</label>
        <select name="lanes[]" multiple class="form-control">
            @foreach ($lanes as $lane)
                <option value="{{$lane->id}}">{{$lane->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Image pour liste</label>
        <div class="custom-file">           
            <input type="file" name="image_list" class="custom-file-input"  d="customFileLang" lang="fr"  accept="image/png" required>
            <label class="custom-file-label" for="customFile">Sélectionner une image</label>
        </div>
    </div>
    <div class="form-group">
        <label>Image splashArt</label>
        <div class="custom-file">           
            <input type="file" name="image_splash" class="custom-file-input"  d="customFileLang" lang="fr"  accept="image/jpg" required>
            <label class="custom-file-label" for="customFile">Sélectionner une image</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block">AJOUTER</button>

    @include('components.errors')

</form>
@endsection