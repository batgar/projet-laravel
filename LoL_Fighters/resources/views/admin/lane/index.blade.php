@extends('layouts.app')

<style>
    .ColSameHeight{
        height: 100%;
    }

    .center-h-v{
        align-items: center; display:flex; justify-content:center;
    } 

    .bg-lane{
        position: relative;
    }

    .bg-lane::before{
        content: "";
        position: absolute;
        top: 0; left: 0;
        width: 100%; height: 100%;
        background-image: url("https://img.redbull.com/images/c_fill,g_auto,w_1500,h_1000/q_auto,f_auto/redbullcom/2015/02/04/1331703315601_2/league-of-legends"); 
        -webkit-filter: brightness(40%);
        background-repeat: no-repeat;
        background-size: cover;
    }
    
    .content{
        position: relative;
    }

    .font-lol{
        font-family: Times, serif;
    }

    .border-gold:hover{
        outline: solid 3px rgb(182,151,87);
        padding: 0;
        margin: 0;
    }

    .bg-title{
        background-color:rgb(0,0,30);
    }

    .btn-size{
        width: 100%;
    }

    .gold-font{
        color: rgb(182,151,87);
    }

    .a-add{
        color:white;
        text-decoration: none;
    }

    .a-add:hover{
        color:white;
        text-decoration: none;
    }
</style>

@section('content')
<div class="row mt-5">
    <div class="col-md-12 mb-4">
        <div class="row">
            @foreach ($lanes as $lane)                    
                <div class="col-md-4 mb-4 ">
                    <div class="card text-center border-gold ColSameHeight bg-lane">
                        <div class="pb-0">
                            <div class="col-md-12 pt-2 pb-2 bg-title">
                                <h4 class=" text-light font-lol mb-0" for="">{{strtoupper($lane->name)}}</h4>
                            </div>
                            <div class="col-md-12 pl-0 pr-0">
                                <div class="">
                                    <div class="content pt-3">
                                        <img class="mt" width="30%"  src="{{asset('storage/'.$lane->image)}}"  alt="">
                                        <div class="row pl-3 pr-3 pt-4">
                                            <div class="col-md-6 text-center ">
                                                <a href="{{route('lane.edit', $lane)}}" style="width: 100%"  type="button" class="btn btn-primary btn-sm btn-size ">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                                                    </svg>
                                                    Modifier
                                                </a>
                                            </div>
                                            <div class="col-md-6 text-center ">
                                                <form action="{{route('lane.delete', $lane->id)}}" method="post">
                                                    @method('DELETE')
                                                    @csrf              
                                                    <button style="width: 100%"  type="submit" class="btn btn-danger btn-sm btn-size">
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                        </svg>
                                                        Supprimer
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-4 mb-4 ">
                <a class="a-add card text-center ColSameHeight border-gold bg-lane" href="{{route('lane.create')}}">
                    <div class="content">
                        <div class="col-md-12 pl-0 pr-0 center-h-v">
                            <h4 class="font-lol bg-title ml-0 mr-0 pt-2 pb-2">AJOUTER UNE NOUVELLE LANE</h4>   
                        </div>
                        <div class="col-md-12 pl-0 pr-0 center-h-v">
                            <svg width="30%"  viewBox="0 0 16 16" class="mt-3 gold-font bi bi-plus-circle-fill mb-5" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                            </svg> 
                        </div>
                    </div>                   
                </a>
            </div>
        </div>
    </div>
</div>
@endsection

