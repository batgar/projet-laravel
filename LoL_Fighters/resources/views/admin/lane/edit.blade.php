@extends('layouts.app')

<style>
 @import "bootstrap/functions";

custom-file-text: (
  en: "Browse",
  es: "Elegir"
);

@import "bootstrap";
</style>

@section('content')
<form class="mt-5" method="post" action="{{route('lane.update', $lane)}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nom de la lane</label>
        <input type="text" name="name" class="form-control" value="{{$lane->name}}" required>
    </div>
    <div class="form-group">
        <label>Image</label>
        <div class="custom-file">           
            <input type="file" name="image" class="custom-file-input"  d="customFileLang" lang="fr"  accept="image/png">
            <label class="custom-file-label" for="customFile">Sélectionner une image</label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block">MODIFIER</button>

    @include('components.errors')

</form>
@endsection