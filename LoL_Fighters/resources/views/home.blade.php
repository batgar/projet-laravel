@extends('layouts.app')

@section('title') Recherche de joueurs @endsection
<style>
    .center-h-v{
        align-items: center; display:flex; justify-content:center;
    } 

    .img-role{
       width: 40%;
    }

    .img-champ{
        width: 60%;
    }

    .img-rank{
        width: 90%;
    }

    .contact-link{
        text-decoration: none;
    }

    .contact-link:hover{
        text-decoration: none;
    }

    .shadow-border{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.20);
        background-color: rgb(240, 240, 240)
        
    }

    .shadow-border:hover{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.3);
    }

</style>

@section('content')
<form onsubmit="return filterUser()" class="row text-center mt-5">       
    <div class="col-md-5">
        <div class="form-group">
            <label>Rôle</label>
            <select id="selectLane" class="form-control shadow-border">
                <option value="0">Tous</option>
                @foreach ($lanes as $lane)
                    <option value="{{$lane->id}}">{{ucfirst($lane->name)}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <label>Élo</label>
            <select id="selectRank" class="form-control shadow-border">
                <option value="0">Tous</option>
                <option value="CHALLENGER">Challenger</option>
                <option value="GRANDMASTER">Grand Maitre</option>
                <option value="MASTER">Maitre</option>
                <option value="DIAMOND">Diamant</option>
                <option value="PLATINUM">Platine</option>
                <option value="GOLD">Or</option>
                <option value="SILVER">Argent</option>
                <option value="BRONZE">Bronze</option>
                <option value="IRON">Fer</option>
            </select>
        </div>
    </div>
    <div class="col-md-2 text-left pt-4 mt-2">
        <button class="btn btn-success" type="submit">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
            </svg>
        </button>
    </div>
</form>
<div class="row">
    @isset($users)
        @foreach ($users as $user)
            @if ($user->username_ig)
            <div class="col-md-12 mb-4">
                <div class="card">
                    <div class="card-body shadow-border">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title d-inline ml-4">Pseudo : {{$user->name}}</h5>
                                        <br>
                                        @if (Auth::user())
                                            @if (Auth::user()->username_ig !== null)
                                                
                                                <a class="contact-link card-title d-inline ml-4" href="{{route('contact',$user->id)}}">
                                                    Contacter
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chat-dots" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                                        <path d="M5 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                                    </svg>
                                                </a>
                                            @endif
                                        @endif

                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="card-title d-inline ml-4">RIOT : {{$user->username_ig}} </h5>
                                    </div>
                                </div>
                                <div class="row">                                  
                                <div class="col-md-4">
                                    <img class="img-rank" src="{{asset('storage/rank/'. Str::slug(explode(" ",$user->rank)[0]) .'.png')}}" alt="">
                                </div>
                                    <div class="col-md-8 text-center">
                                        <div class="w-100 mt-3">
                                            <label class="" for="">{{$user->rank}} </label>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-2 pl-0 pr-0">
                                                        <p>W {{$user->nb_victoire}}</p>
                                                    </div>
                                                    <div class="col-md-8  pt-1">
                                                        <div class="progress" style="border: solid 1px black">
                                                            <div class="progress-bar bg-success" role="progressbar" style="width:{{(($user->nb_victoire/($user->nb_victoire+$user->nb_defaite))*100)}}%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                            <div class="progress-bar bg-danger" role="progressbar" style="width:{{(($user->nb_defaite/($user->nb_victoire+$user->nb_defaite))*100)}}%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 pl-0 pr-0">
                                                        <label for="">L {{$user->nb_defaite}}</label>
                                                    </div>
                                                </div> 
                                            </div> 
                                            <label for="">{{intval((($user->nb_victoire/($user->nb_victoire+$user->nb_defaite))*100))}}% de victoire</label><br>
                                            <label for="">{{$user->nb_victoire + $user->nb_defaite}} partie joué</label>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-md-3 text-center center-h-v">
                                <img class="img-role" src="{{asset('storage/'. $user->lane->image)}}" alt="">
                            </div> 
                            <div class="col-md-3 text-center center-v-h">
                                <img class="img-champ rounded-circle shadow-border" src="{{asset('storage/'. $user->champion->image_list)}}" alt="">
                                <br>
                                <label class="mt-2 mb-0" for="">{{ucfirst($user->champion->name)}}</label> 
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    @endisset
</div>
@endsection

<script>

function filterUser(){
    let idLane = $("#selectLane").val();
    let rank = $("#selectRank").val();

    window.location.href = "{{route('home')}}/" + idLane + "/" + rank;
    return false
}

</script>