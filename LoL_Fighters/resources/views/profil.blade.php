@extends('layouts.app')

<style>
    .center-h-v{
        align-items: center; display:flex; justify-content:center;
    } 

    .img-role{
       width: 40%;
    }

    .img-champ{
        width: 60%;
    }

    .img-rank{
        width: 90%;
    }

    .contact-link{
        cursor: pointer;
    }

    .shadow-border{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.20);
        background-color: rgb(240, 240, 240)
        
    }

    .shadow-border:hover{
        box-shadow: 0 0 30px -4px rgba(0, 0, 0, 0.3);
    }

</style>

@section('content') 
<div class="row mt-5">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body shadow-border">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="card-title d-inline ml-4">Pseudo : {{Auth::user()->name}}</h5>
                            </div>
                            <div class="col-md-6">
                                @if (Auth::user()->username_ig)
                                    <h5 class="card-title d-inline ml-4">RIOT : {{Auth::user()->username_ig}} </h5>
                                @else
                                    <label for="">Ajouter ou modifier votre pseudo RIOT pour accéder aux informations</label>
                                @endif
                                
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-4">   
                                @if (Auth::user()->username_ig)                            
                                    <img class="img-rank" src="{{asset('storage/rank/'. Str::slug(explode(" ",Auth::user()->rank)[0]) .'.png')}}" alt="">
                                @endif                               
                            </div>
                            <div class="col-md-8 text-center">
                                <div class="w-100 mt-3">
                                    @if (Auth::user()->username_ig)
                                        <label class="" for="">{{Auth::user()->rank}} </label>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2 pl-0 pr-0">
                                                @if (Auth::user()->username_ig)
                                                    <p>W {{Auth::user()->nb_victoire}}</p>
                                                @endif
                                            </div>
                                            <div class="col-md-8  pt-1">
                                                @if (Auth::user()->username_ig)
                                                    <div class="progress" style="border: solid 1px black">
                                                        <div class="progress-bar bg-success" role="progressbar" style="width:{{((Auth::user()->nb_victoire/(Auth::user()->nb_victoire+Auth::user()->nb_defaite))*100)}}%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                                        <div class="progress-bar bg-danger" role="progressbar" style="width:{{((Auth::user()->nb_defaite/(Auth::user()->nb_victoire+Auth::user()->nb_defaite))*100)}}%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-2 pl-0 pr-0">
                                                @if (Auth::user()->username_ig)
                                                    <label for="">L {{Auth::user()->nb_defaite}}</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @if (Auth::user()->username_ig)
                                        <label for="">{{intval(((Auth::user()->nb_victoire/(Auth::user()->nb_victoire+Auth::user()->nb_defaite))*100))}}% de victoire</label><br>
                                        <label for="">{{Auth::user()->nb_victoire + Auth::user()->nb_defaite}} partie joué</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="col-md-3 text-center center-h-v">
                        @if (Auth::user()->lane !== null)
                            <img class="img-role" src="{{asset('storage/'.Auth::user()->lane->image)}}" alt="">
                        @else
                            <label for="">Aucune donnée sur votre lane.</label>
                        @endif
                    </div> 
                    <div class="col-md-3 text-center center-v-h">
                        @if (Auth::user()->champion !== null)
                            <img class="img-champ rounded-circle shadow-border" src="{{asset('storage/'.Auth::user()->champion->image_list)}}" alt="">
                            <br>
                            <label class="mt-2 mb-0" for="">{{ucfirst(Auth::user()->champion->name)}}</label>
                        @else
                            <label for="">Aucune donnée sur votre champion.</label>
                        @endif 
                    </div> 
                </div>
                <div id="btn-modif" class="row text-center">
                    <div class="col-md-12">
                        <form action="{{route('profil.update',Auth::user()->id)}}" method="post" class="d-inline"> 
                            @csrf         
                            <button  type="submit" class="btn btn-primary btn-sm btn-size">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-repeat" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
                                    <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
                                </svg>
                                Mettre a jour le profil
                            </button>
                            @include('components.errors')
                        </form>
                        <button onclick="showform()" type="button" class="btn btn-primary btn-sm btn-size">  
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-gear-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 0 0-5.86 2.929 2.929 0 0 0 0 5.858z"/>
                            </svg>
                            Modifier            
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modif-profil" class="row">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body shadow-border">
                <div class=" col-md-12 text-right">
                    <div onclick="hideform()" id="close-form" class="btn">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="text-danger bi bi-x-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                            <path fill-rule="evenodd" d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                        </svg>
                    </div>
                </div>
                <form class="" method="post" action="{{route('profil.update',Auth::user()->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Pseudo RIOT</label>
                        <input type="text" name="name_ig"  @if (Auth::user()->username_ig !== null) value="{{Auth::user()->username_ig}}" @endif class="form-control" required>
                        @if (isset($error))
                            <label class="text-danger sm">Pseudo RIOT incorrect</label>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Rôle</label>
                        <select name="lane" class="form-control shadow-border" id="exampleFormControlSelect1">
                            @foreach ($lanes as $lane)
                                @if (Auth::user()->lane !== null)
                                    @if (Auth::user()->lane->name === $lane->name)
                                        <option selected value="{{$lane->id}}">{{$lane->name}}</option>
                                    @else
                                        <option value="{{$lane->id}}">{{$lane->name}}</option>
                                    @endif  
                                @else
                                    <option value="{{$lane->id}}">{{$lane->name}}</option>
                                @endif    
                            @endforeach   
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Champion</label>
                        <select name="champion" class="form-control shadow-border" id="exampleFormControlSelect1">
                            @foreach ($champions->sortBy('name') as $champion)
                                @if (Auth::user()->champion !== null)
                                    @if (Auth::user()->champion->name === $champion->name)
                                        <option selected value="{{$champion->id}}">{{$champion->name}}</option>
                                    @else
                                        <option value="{{$champion->id}}">{{$champion->name}}</option>
                                    @endif 
                                @else 
                                    <option value="{{$champion->id}}">{{$champion->name}}</option>
                                @endif
                            @endforeach    
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">MODIFIER</button>
                
                    @include('components.errors')
                
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="error" value="@if (isset($error)){{$error}}@endif">

@endsection
@section('script')
    
<script>
    if(document.getElementById('error').value == 1)
    {
        document.getElementById('modif-profil').hidden = false;
        document.getElementById('btn-modif').hidden = true;
    }else{
        document.getElementById('modif-profil').hidden = true;
        document.getElementById('btn-modif').hidden = false;
    }

    function showform(){
        document.getElementById('modif-profil').hidden = false;
        document.getElementById('btn-modif').hidden = true;
    }

    function hideform(){
        document.getElementById('modif-profil').hidden = true;
        document.getElementById('btn-modif').hidden = false;
    }
    
</script>
@endsection
