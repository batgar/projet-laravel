{{-- 
Hi ! <br><br>

Je m'appelle {{$data['name']}} et mon message est : <br>
 --}}


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>INVITATION A JOUER!</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			* {
				-ms-text-size-adjust:100%;
				-webkit-text-size-adjust:none;
				-webkit-text-resize:100%;
				text-resize:100%;
			}
			a{
				outline:none;
				color:#fff;
				text-decoration:underline;
			}
			a:hover{text-decoration:none !important;}
			a[x-apple-data-detectors]{
				color:inherit !important;
				text-decoration:none !important;
			}
			.active:hover{opacity:0.8;}
			.btn,
			.active{
				-webkit-transition:all 0.3s ease;
				 -moz-transition:all 0.3s ease;
					-ms-transition:all 0.3s ease;
						transition:all 0.3s ease;
			}
			.btn:hover{background:rgba(255, 255, 255, 0.14);}
			table td{border-collapse:collapse !important; mso-line-height-rule:exactly;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
			@media only screen and (max-width:500px) {
				/* default style */
				table[class="flexible"]{width:100% !important;}
				*[class="hide"]{
					display:none !important;
					width:0 !important;
					height:0 !important;
					padding:0 !important;
					font-size:0 !important;
					line-height:0 !important;
				}
				td[class="img-flex"] img{width:100% !important; height:auto !important;}
				td[class="aligncenter"]{text-align:center !important;}
				th[class="flex"]{display:block !important; width:100% !important;}
				tr[class="table-holder"]{display:table !important; width:100% !important;}
				th[class="thead"]{display:table-header-group !important; width:100% !important;}
				th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
				/* custom style */
				td[class="wrapper"]{padding:0 !important;}
				td[class="header"]{padding:20px 10px 30px !important;}
				td[class="indent-link"]{
					text-align:center !important;
					padding:0 0 30px !important;
				}
				td[class="h-auto"]{height:auto !important;}
				td[class="frame"]{padding:20px 10px !important;}
				td[class="p-0"]{padding:0 !important;}
				td[class="product-holder"]{padding:30px 10px !important;}
				td[class="cite-box"]{padding:20px 10px !important;}
				td[class="cite-box"] *{text-align:center !important;}
				td[class="btn-indent"]{padding:20px 0 0 !important;}
				td[class="w-10"]{width:10px !important;}
			}
		</style>
	</head>
	<body style="margin:0; padding:0;" bgcolor="#313d5a">
		<table  style="min-width:320px; margin-top:50px; margin-bottom:50px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#313d5a">
			<!-- fix for gmail -->
			<tr>
				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
			<!-- fix for gmail -->
			<tr>
				<td class="hide">
					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
						<tr>
							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<!-- header -->
					
					<!-- section-01 -->
					<table data-module="banner" data-thumb="thumbnails/img-02.jpg" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="wrapper" style="padding:0 10px;" data-bgcolor="bg module" bgcolor="#313d5a">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="img-flex" style="text-align: center"><a href="#" ><img src="{{asset('storage/logo/logo2.png')}}" width="50%" border="0" style="vertical-align:top;" alt="" /></a></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- section-02 -->
					<table data-module="article-01" data-thumb="thumbnails/img-03.jpg" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="wrapper" style="padding:0 10px;" data-bgcolor="bg module" bgcolor="#313d5a">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td class="frame" data-bgcolor="bg block" bgcolor="#0b1016" style="padding:58px 50px 29px;">
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr>
																<td class="cite-box" style="border-radius:3px; padding:24px; border:1px solid #fff;">
																	<table width="100%" cellpadding="0" cellspacing="0">
																		<tr>
																			<td data-color="title" data-size="size title" data-min="10" data-max="26" data-link-color="link text color" data-link-style="text-decoration:none; color:#7ea0b7;" align="center" style="padding:0 0 10px; font:bold 24px/26px Arial, Helvetica, sans-serif; color:#7ea0b7; text-transform: uppercase;">INVITATION A JOUER!</td>
																		</tr>
																		<tr>
                                                                            <td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="text-decoration:underline; color:#fff;" style="font:12px/18px Arial, Helvetica, sans-serif; color:#fefefe;">
                                                                                Salut <span style="font-weight: bold; display-inline:none;font-size:15px;">{{$data['nametosend']}}</span> ! <br>
                                                                                <span style="font-weight: bold;display-inline:none;font-size:17px;">{{$data['name']}}</span> vous invite a jouer. <br>
                                                                                Ajouter <span style="font-weight: bold; display-inline:none;font-size:17px;">{{$data['username_ig']}}</span> a vos amis pour jouer ensemble.
                                                                                @if ($data['message'])
                                                                                <br><br>
                                                                                Il vous a laissé un message : 
                                                                                <hr>
                                                                                {{$data['message']}}
                                                                                @endif
                                                                            </td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>