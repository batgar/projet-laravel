<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/search/{lane?}/{rank?}', 'HomeController@index')->name('home');

Route::get('champion/search/{lane?}/{category?}/{faction?}', 'ChampionController@index')->name('champion');
Route::get("champion/details/{champion}", "ChampionController@show")->name('detailsChampion');

Route::get("profil", "ProfilController@index")->middleware('auth')->name('profil');
Route::post("profil/update/{user}", "ProfilController@update")->middleware('goodUser')->name('profil.update');

Route::middleware("admin")->group(function () {

    Route::get("admin/champion/edit/{champion}", "ChampionController@edit")->name('champion.edit');
    Route::post("admin/champion/update/{champion}", "ChampionController@update")->name('champion.update');
    Route::get("admin/champion/create", "ChampionController@create")->name('champion.create');
    Route::post("admin/champion/store", "ChampionController@store")->name('champion.store');
    Route::delete("admin/champion/delete/{champion}", "ChampionController@delete")->name('champion.delete');
    
    Route::get("admin/lane", "LaneController@index")->name('lane');
    Route::get("admin/lane/create", "LaneController@create")->name('lane.create');
    Route::post("admin/lane/store", "LaneController@store")->name('lane.store');
    Route::get("admin/lane/edit/{lane}", "LaneController@edit")->name('lane.edit');
    Route::post("admin/lane/update/{lane}", "LaneController@update")->name('lane.update');
    Route::delete("admin/lane/delete/{lane}", "LaneController@delete")->name('lane.delete');
    
    Route::get("admin/faction", "FactionController@index")->name('faction');
    Route::get("admin/faction/create", "FactionController@create")->name('faction.create');
    Route::post("admin/faction/store", "FactionController@store")->name('faction.store');
    Route::get("admin/faction/edit/{faction}", "FactionController@edit")->name('faction.edit');
    Route::post("admin/faction/update/{faction}", "FactionController@update")->name('faction.update');
    Route::delete("admin/faction/delete/{faction}", "FactionController@delete")->name('faction.delete');
    
    Route::get("admin/category", "CategoryController@index")->name('category');
    Route::get("admin/category/create", "CategoryController@create")->name('category.create');
    Route::post("admin/category/store", "CategoryController@store")->name('category.store');
    Route::get("admin/category/edit/{category}", "CategoryController@edit")->name('category.edit');
    Route::post("admin/category/update/{category}", "CategoryController@update")->name('category.update');
    Route::delete("admin/category/delete/{category}", "CategoryController@delete")->name('category.delete');
    
    
    Route::get("admin/user", "UserAdminController@index")->name('user.index');
    Route::delete("admin/user/delete/{user}", "UserAdminController@delete")->name('user.delete');
    Route::post("admin/user/update/{user}", "UserAdminController@updateAdmin")->name('user.update');
});
Route::get('contact/{user}', 'ContactController@index')->name('contact')->middleware('auth', 'have_username_ig');
Route::post('contact', 'ContactController@email')->name('sendMailContact')->middleware('auth', 'have_username_ig');

Auth::routes();
